
INFO
===============================================================================

Module: Resubmission
Author: Andre Schuetz (Faustas)
Drupal: 6

OVERVIEW
===============================================================================

The resubmission module makes it possible to monitore specific nodes. 
The nodes are presented to the user in a list ordered by their resubmission 
time. If a node nears his resubmission day, the user can make an update of the 
monitored content.
The user has the opportunity to set a resubmission interval or select a date
for the resubmission of the content.

This is especially useful when the content of a node must be checked at specific 
time intervals. 

An example could be the control of website content, eg the website content
contains weekly changing offers. 

INSTALL
===============================================================================

Download the module and unpack to the /sites/all/modules folder.

CONFIGURATION
===============================================================================

You can change the default setting at:

admin/content/resubmission/settings

Specify the node types that are possible to add to the resubmission list and
set the default resubmission time interval.

The list with the monitored nodes is at:

admin/content/resubmission

If you want to add a node to the resubmission list, you have to open the node
'edit' form. Open the fieldset with the 'Resubmission' name and activate the
checkbox.
