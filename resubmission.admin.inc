<?php

/**
 * @file
 *
 * This file contains the admin section for the
 * resubmission settings.
 *
 * @author Faustas <as@faustas.de>
 *
 */

/**
 * Resubmission settings dialog
 *
 */
function resubmission_admin_settings($form_state) {
  $form = array();

  $form['use_resubmission'] = array(
    '#title' => t('Use resubmission'),
    '#description' => t('This is the general activation checkbox for the resubmission functionality.'),
    '#type' => 'checkbox',
    '#default_value' => (int)variable_get('use_resubmission', 0),
  );

  //
  // default resubmission interval
  //
  $form['resubmission_di'] = array(
    '#title' => t('Default resubmission interval'),
    '#description' => t('Set the default interval for the resubmissions. The interval is given in days.'),
    '#type' => 'fieldset',
  );

  $intervals = range(0, 50);
  unset($intervals[0]);
  $form['resubmission_di']['resubmission_default_interval'] = array(
    '#title' => t('interval (in days)'),
    '#type' => 'select',
    '#options' => $intervals,
    '#default_value' => (int)variable_get('resubmission_default_interval', 7),
  );

  //
  // selectable node types
  //
  $form['resubmission_nt'] = array(
    '#title' => t('Selectable node types'),
    '#description' => t('Select the node types that can be resubmitted.'),
    '#type' => 'fieldset',
  );

  $types = node_get_types();
  foreach ($types as $node ) {
    $selectable_node_types[$node->type] = t('!name (!desc)',
      array('!name' => $node->name, '!desc' => $node->description));
  }
  $form['resubmission_nt']['resubmission_selected_node_types'] = array(
    '#title' => '',
    '#type' => 'checkboxes',
    '#options' => $selectable_node_types,
    '#default_value' => (array)variable_get('resubmission_selected_node_types', array()),
  );

  return system_settings_form($form);
}// _admin_settings
